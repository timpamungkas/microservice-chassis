# Build command
#
# 1. gradle clean bootJar
# 2. docker build --label rabbit --tag rabbit:1.5 . 
# 3. docker login
# 4. docker tag rabbit:1.5 timpamungkas/rabbit:1.5
# 5. docker push timpamungkas/rabbit:1.5
# gradle clean bootJar && docker build --label rabbit --tag rabbit:1.5 . && docker tag rabbit:1.5 timpamungkas/rabbit:1.5 && docker push timpamungkas/rabbit:1.5

# Start with a base image containing Java runtime
FROM openjdk:11-jre-stretch	

# Add Maintainer Info
LABEL maintainer="timotius.pamungkas@gmail.com"

# add cacerts & config file
COPY cacerts /docker-java-home/lib/security

# put rabbit.yml here for external config
RUN mkdir conf

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8002 available to the world outside this container
EXPOSE 8002

# The application's jar file
ARG JAR_FILE=build/libs/rabbit-1.5.jar

# Add the application's jar to the container
ADD ${JAR_FILE} rabbit.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/rabbit.jar"]