package com.course.microservice.scheduler;

import java.time.LocalDateTime;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DummyScheduler {

	@Scheduled(fixedDelay = 10000)
	public void dummy() {
		System.out.println(LocalDateTime.now());
	}

	@Scheduled(cron = "0 0 13 * * *")
	public void dummyCron() {
		System.out.println(LocalDateTime.now());
	}

}
