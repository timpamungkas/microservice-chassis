package com.course.microservice.api.request;

public class MyBody {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
